<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'I<QpOxB){Dx&3wK@a:B6M6AGd:tY#BF//@5j+zURc+>E8e^Qz{BbHc,F^FzlDvk]');
define('SECURE_AUTH_KEY',  's7*+E=KcF]%qNHu/imVoVFHX ?]}w(xZ$Rn]mHy7.N?ZF2IC:C8|kp9=mtNs21p0');
define('LOGGED_IN_KEY',    '(gqxbRL|c}bJM-*Yz^G:L,7M-u;yUm}uOIv;+i%h<8bK)3k@Nua:vqtm<kSB7j<2');
define('NONCE_KEY',        'I[q`&mYmnAmV]5z:YUQAmi;_P@ 5-+{eg Agff{Q0u[a5,Bl=e`i`~-wV9il[:t=');
define('AUTH_SALT',        'bp(uG*)&*FZ).J(o>IMKD%Q+-ZFNaUg1V/(gT{;tiT9_E/G^BR!@mkXdZM .NU2E');
define('SECURE_AUTH_SALT', 'S!oxb</YIc2 .-m{xm8IjbdNS.`G`z65[@:IFPET/.$#z>7R-! wEmUNLl?yTkoV');
define('LOGGED_IN_SALT',   'm=AyjQDwl@_65r_4D<^XBy.!bkp6jm[k_0hf:ggl$+ (Aef$]geU&.zT[4ROGyF$');
define('NONCE_SALT',       'kqZYZ<NwF%xoI4|Wd 1+k25A?>rU6XZm4#gW$muh]46^B@6[y0*Pwgg@FW;7{DXC');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
